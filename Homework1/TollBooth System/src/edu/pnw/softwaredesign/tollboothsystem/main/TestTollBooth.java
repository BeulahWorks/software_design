package edu.pnw.softwaredesign.tollboothsystem.main;

import edu.pnw.softwaredesign.tollboothsystem.tollbooth.AlleghenyTollBooth;
import edu.pnw.softwaredesign.tollboothsystem.tollbooth.ITollBooth;
import edu.pnw.softwaredesign.tollboothsystem.truck.DaewooTruck;
import edu.pnw.softwaredesign.tollboothsystem.truck.FordTruck;
import edu.pnw.softwaredesign.tollboothsystem.truck.ITruck;
import edu.pnw.softwaredesign.tollboothsystem.truck.NissanTruck;

/**
 * Test the toll booth system.
 * @author Rui Zhang
 * @version 1.0
 * @since 1/20/2017
 */
public class TestTollBooth {

  /**
   * Initial and test toll booth system.
   * @param args default input for main.
   * @throws Exception the input of truck data should larger than 0.
   */
  public static void main(String[] args) throws Exception {
    ITollBooth booth = new AlleghenyTollBooth();

    ITruck ford = new FordTruck(5, 12000); // 5 axles and 12000 kilograms
    ITruck nissan = new NissanTruck(2, 5000); // 2 axles and 5000kg
    ITruck daewoo = new DaewooTruck(6, 17000); // 6 axles and 17000kg

    booth.calculateToll(ford);
    booth.displayData();
    booth.calculateToll(nissan);

    booth.resetData();
    booth.calculateToll(daewoo);
  }

}
