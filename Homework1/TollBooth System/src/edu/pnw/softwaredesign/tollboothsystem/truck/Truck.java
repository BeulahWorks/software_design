package edu.pnw.softwaredesign.tollboothsystem.truck;

/**
 * The abstract truck model
 * @author Rui Zhang
 * @version 1.0
 * @since 1/20/2017
 */
public class Truck implements ITruck {
  private int axlesNum;
  private int weight;

  /**
   * Build a Truck object.
   * 
   * @param axlesNum the axles number of the truck.
   * @param weight the weight of the truck.
   * @throws Exception the input of truck data should larger than 0.
   */
  public Truck(int axlesNum, int weight) throws Exception {
    if (axlesNum > 0 && weight > 0) {
      this.axlesNum = axlesNum;
      this.weight = weight;
    } else {
      throw new Exception("Error: the data of truck is incorrect.");
    }
  }

  @Override
  public int getWeight() {
    return this.weight;
  }

  @Override
  public int getAxlesNum() {
    return this.axlesNum;
  }

}
