package edu.pnw.softwaredesign.tollboothsystem.truck;

/**
 * The daewoo truck model.
 * @author Rui Zhang
 * @version 1.0
 * @since 1/20/2017
 */
public class DaewooTruck extends Truck {

  public DaewooTruck(int axlesNum, int weight) throws Exception {
    super(axlesNum,weight);
  }

}
