package edu.pnw.softwaredesign.tollboothsystem.truck;

/**
 * The interface of truck.
 * @author Rui Zhang
 * @version 1.0
 * @since 1/20/2017
 */
public interface ITruck {
  /**
   * Return the weight of truck.
   * @return the weight of truck.
   */
  abstract int getWeight();

  /**
   * Return the axles number of truck.
   * @return the axles number of truck.
   */
  abstract int getAxlesNum();
}
