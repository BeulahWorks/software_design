package edu.pnw.softwaredesign.tollboothsystem.truck;

/**
 * The nissan truck model
 * @author Rui Zhang
 * @version 1.0
 * @since 1/20/2017
 */
public class NissanTruck extends Truck {
  /**
   * Build a NissanTruck object.
   * @param axlesNum the axles number of the truck.
   * @param weight the weight of the truck.
   * @throws Exception the input of truck data should larger than 0.
   */
  public NissanTruck(int axlesNum, int weight) throws Exception {
    super(axlesNum,weight);
  }
  
}
