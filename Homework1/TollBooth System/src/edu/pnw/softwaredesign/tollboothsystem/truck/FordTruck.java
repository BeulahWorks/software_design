package edu.pnw.softwaredesign.tollboothsystem.truck;

/**
 * The ford truck model.
 * @author Rui Zhang
 * @version 1.0
 * @since 1/20/2017
 */
public class FordTruck extends Truck {
  
  public FordTruck(int axlesNum, int weight) throws Exception {
    super(axlesNum, weight);
  }
}
