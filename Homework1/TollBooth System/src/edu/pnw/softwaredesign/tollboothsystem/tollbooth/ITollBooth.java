package edu.pnw.softwaredesign.tollboothsystem.tollbooth;

import edu.pnw.softwaredesign.tollboothsystem.truck.ITruck;

/**
 * The toll booth system interface
 * @author Rui Zhang
 * @version 1.0
 * @since 1/20/2017
 */
public interface ITollBooth {

  /**
   * Calculate the current truck cost, 
   * and add this cost to total cost,
   * increase the total pass truck.
   * @param truck Reference of current truck
   */
  abstract void calculateToll(ITruck truck);

  /**
   * Display the current summary of cost.
   */
  abstract void displayData();

  /**
   * Display the summary and reset the data.
   */
  abstract void resetData();
  
  /**
   * Get total truck number.
   * @return total truck number.
   */
  abstract  int getTotalTruckNum();

  /**
   * Get total receipt.
   * @return total receipt.
   */
  abstract  int getTotalReceipt();
}
