package edu.pnw.softwaredesign.tollboothsystem.tollbooth;
/**
 * The abstract TollBooth model
 * @author Rui Zhang
 * @version 1.0
 * @since 1/20/2017
 */

import edu.pnw.softwaredesign.tollboothsystem.truck.ITruck;

public class TollBooth implements ITollBooth {
  private int totalTruckNum;
  private int totalReceipt;
  private int pricePerAxles = 5;
  private int pricePerTonOfTruck = 10;
  private int singleTruckReceipt;

  public TollBooth() {
    this.totalTruckNum = 0;
    this.totalReceipt = 0;
  }

  @Override
  public void calculateToll(ITruck truck) {
    this.singleTruckReceipt = truck.getAxlesNum() * this.pricePerAxles
        + truck.getWeight() * this.pricePerTonOfTruck / 1000;
    this.totalReceipt += this.singleTruckReceipt;
    this.totalTruckNum += 1;
    System.out.println("Truck arrival - Axles: " + truck.getAxlesNum() + " Total weight: "
        + truck.getWeight() + " Toll due: $" + this.singleTruckReceipt);
  }

  @Override
  public void displayData() {
    System.out.println("Totals since last collection - Receipts: $" + this.totalReceipt
        + "  Trucks: " + this.totalTruckNum);
  }

  @Override
  public void resetData() {
    System.out.println("*** Collecting receipts  ***");
    System.out.println("Totals since last collection - Receipts: $" + this.totalReceipt
        + "  Trucks: " + this.totalTruckNum);
    this.totalTruckNum = 0;
    this.totalReceipt = 0;
  }

  @Override
  public int getTotalTruckNum() {
    return totalTruckNum;
  }

  @Override
  public int getTotalReceipt() {
    return totalReceipt;
  }

}
