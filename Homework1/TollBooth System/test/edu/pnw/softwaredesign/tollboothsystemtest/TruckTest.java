package edu.pnw.softwaredesign.tollboothsystemtest;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import edu.pnw.softwaredesign.tollboothsystem.tollbooth.AlleghenyTollBooth;
import edu.pnw.softwaredesign.tollboothsystem.tollbooth.ITollBooth;
import edu.pnw.softwaredesign.tollboothsystem.truck.DaewooTruck;
import edu.pnw.softwaredesign.tollboothsystem.truck.FordTruck;
import edu.pnw.softwaredesign.tollboothsystem.truck.ITruck;
import edu.pnw.softwaredesign.tollboothsystem.truck.NissanTruck;

import org.junit.Test;


public class TruckTest {

  @Test
  public void daewooTruckTest() throws Exception {
    ITruck daewooTruck = new DaewooTruck(2, 15000);
    assertEquals(2, daewooTruck.getAxlesNum());
    assertEquals(15000, daewooTruck.getWeight());
  }

  @Test
  public void fordTruckTest() throws Exception {
    ITruck fordTruck = new FordTruck(2, 15000);
    assertEquals(2, fordTruck.getAxlesNum());
    assertEquals(15000, fordTruck.getWeight());
  }

  @Test
  public void nissanTruckTest() throws Exception {
    ITruck nissanTruck = new NissanTruck(2, 15000);
    assertEquals(2, nissanTruck.getAxlesNum());
    assertEquals(15000, nissanTruck.getWeight());
  }

  @Test
  public void testNegativeCalculateToll() throws Exception {
    ITollBooth tollBooth = new AlleghenyTollBooth();
    try {
      ITruck ford = new FordTruck(-5, 12000);
      tollBooth.calculateToll(ford);
      fail();
    } catch (Exception exception) {
      assertTrue(true);
    }
  }

  @Test
  public void testResetData() throws Exception {
    ITollBooth tollBooth = new AlleghenyTollBooth();
    ITruck ford = new FordTruck(2, 15000);
    tollBooth.calculateToll(ford);
    tollBooth.resetData();
    assertEquals(0, tollBooth.getTotalReceipt());
    assertEquals(0, tollBooth.getTotalTruckNum());
  }
  
  @Test
  public void testCalculateToll() throws Exception {
    ITollBooth tollBooth = new AlleghenyTollBooth();
    ITruck ford = new FordTruck(5, 12000);
    tollBooth.calculateToll(ford);
    assertEquals(145, tollBooth.getTotalReceipt());
    assertEquals(1, tollBooth.getTotalTruckNum());
  }
}
