package edu.pnw.softwaredesign.animal;

public abstract class Animal {
  abstract void makeSound();
}
