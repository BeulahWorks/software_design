package edu.pnw.softwaredesign.animal;

public interface IRunnable {
  abstract void run();
}
