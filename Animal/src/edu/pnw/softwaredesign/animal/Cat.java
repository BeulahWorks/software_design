package edu.pnw.softwaredesign.animal;



public class Cat extends Animal implements IRunnable {
  // Cat property.
  private int weight;
  private Color furColor = new Color();
  public House catHouse;
  public CatHead head;
  
  /** 
   * The constructor for a new cat
   * @param catHead the cat head
   */
  public Cat(CatHead catHead) {
    super();
    this.head = catHead;

  }

  public int getWeight() {
    return weight;
  }

  public void setWeight(int weight) {
    this.weight = weight;
  }

  public void play(Ball ball) {

  }

  public void fight(Dog dog) {

  }

  public void run() {
    // TODO Auto-generated method stub

  }

  public void makeSound() {
    // TODO Auto-generated method stub

  }



}
